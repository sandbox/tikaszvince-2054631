/**
 * @file
 * REP Map image JS behavior.
 */

(function($){
  Drupal.behaviors.gmap_image = {
    attach : function (context){
      if (Drupal.behaviors.gmap_image.is_retina()) {
        $('.map-object[data-retina-src]').each(function(){
          this.src = $(this).attr('data-retina-src');
        });
      }
    },

    is_retina: function(){
      if (window.devicePixelRatio > 1) {
        return true;
      }
      var mediaQuery =
        "(-webkit-min-device-pixel-ratio: 1.5),\
        (min--moz-device-pixel-ratio: 1.5),\
        (-o-min-device-pixel-ratio: 3/2),\
        (min-resolution: 1.5dppx)";
      return window.matchMedia && window.matchMedia(mediaQuery).matches;
    }
  }
})(jQuery);
