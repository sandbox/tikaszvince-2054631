<?php
/**
 * @file
 * Hooks provided by the gmap_image module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alters the map image url params.
 *
 * @param array $url
 *   The map image url params.
 */
function hook_gmap_image_url_alter(&$url) {
  $url['query']['zoom'] = 16;
  $url['query']['marker'] = 'color:green|label:M';
}

/**
 * @} End of "addtogroup hooks".
 */
