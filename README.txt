print theme('gmap_image', array(
  'zoom' => 14,
  'location' => 'Árpád út 51-53, Budapest, Hungary',
  'size' => array(
    'width' => 400,
    'height' => 300,
  ),
  'marker' => 'color:red',
));
